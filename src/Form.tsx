import React, { FC, useState } from 'react';
import { TextField } from '@material-ui/core';

interface ToDoFormProps{
    saveTodo: (t: string) => void;
}

const ToDoForm: FC<ToDoFormProps> = ({saveTodo}) => {
    const [value, setValue] = useState<string>('')
    return (
        <form 
            onSubmit={(event) => {
                event.preventDefault()
                saveTodo(value)
                setValue('')
            }}>
            <TextField 
                id="outlined-basic" 
                label="todo-application" 
                variant="outlined"
                value={value} 
                onChange={(e) => setValue(e.target.value)}
            />
        </form>
    );
};

export default ToDoForm;