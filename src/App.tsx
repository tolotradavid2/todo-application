import { Typography } from '@material-ui/core';
import React, { useState } from 'react';
import ToDoForm from './Form';

function App() {

  const [tasks, setTasks] = useState<String[]>([])

  return (
    <div
      style={{
        textAlign: 'center',
        fontFamily: 'sans-serif'
      }}
    >
      <Typography
        component='h1'
        variant='h2'
      >
        Todos
      </Typography>
      <ToDoForm 
        saveTodo={(t) => {
          setTasks([...tasks, t.trim()])
        }}
        />
      <Typography>
        {tasks}
      </Typography>

    </div>
  );
}

export default App;